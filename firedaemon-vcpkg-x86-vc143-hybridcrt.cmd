title %~nx0

set VCPKG_DISABLE_METRICS=1
set VCPKG_FEATURE_FLAGS=-binarycaching
set VCPKG_DEFAULT_TRIPLET=x86-windows-vc143-hybridcrt
set VCPKG_OVERLAY_TRIPLETS=firedaemon\triplets
set VCPKG_OVERLAY_PORTS=firedaemon\ports

pushd ..
vcpkg env --triplet %VCPKG_DEFAULT_TRIPLET% --overlay-triplets="%VCPKG_OVERLAY_TRIPLETS%" --overlay-ports="%VCPKG_OVERLAY_PORTS%"
