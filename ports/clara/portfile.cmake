vcpkg_from_github(
    OUT_SOURCE_PATH SOURCE_PATH
    REPO trueqbit/Clara
    REF v1.1.5.2
    SHA512 e5fa37c6832654d24083e9ea17627921b13e6912af38258a57d7057c07cecd7540094d6a742f792c519fe3c89f3c892a901171c7c31268401521649a3d907ed5
    HEAD_REF subcommand
)
file(INSTALL ${SOURCE_PATH}/single_include/clara.hpp DESTINATION ${CURRENT_PACKAGES_DIR}/include)
file(INSTALL ${SOURCE_PATH}/single_include/clara.hpp DESTINATION ${CURRENT_PACKAGES_DIR}/share/clara RENAME copyright)
vcpkg_copy_pdbs()