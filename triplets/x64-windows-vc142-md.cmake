set(VCPKG_TARGET_ARCHITECTURE x64)
set(VCPKG_PLATFORM_TOOLSET v142)
set(VCPKG_CRT_LINKAGE dynamic)
set(VCPKG_LIBRARY_LINKAGE dynamic)
set(VCPKG_CXX_FLAGS "/Zc:__cplusplus /Zc:throwingNew")
set(VCPKG_C_FLAGS "")
set(VCPKG_ENV_PASSTHROUGH VCPKG_DEFAULT_TRIPLET;VCPKG_OVERLAY_TRIPLETS;VCPKG_OVERLAY_PORTS)
set(VCPKG_ENV_PASSTHROUGH_UNTRACKED VCPKG_FEATURE_FLAGS)
